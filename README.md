#  INA

This project can be summarized in three words: **I**ntelligente, **N**untium, **A**ffectus (or Understand, News, Feelings).

Attempting to objectively understand how people feel about the news.

---
Repository created in partial fulfillment of the requirements for KEN2570 Natural Language Processing course at Data Science and Knowledge Engineering, Maastricht University.

---
Visit https://p-skaisgiris.gitlab.io/ina-website/ for more information about this project and some example results.

---

#### Contributions of this project
- Script to annotate datasets for Aspect-Category Sentiment Analysis `annotating_nyt_dataset.py` (at the moment works best for [New York Times dataset](https://www.kaggle.com/aashita/nyt-comments))
- Manually annotated novel dataset `datasets/nyt-train.txt` and `datasets/nyt-test.txt` of The New York Times (NYT) comments 
- Trained BERT model (`absa_pytorch_utils/pretrained_bert_model/aen_bert_nyt_test_acc0.678`) (at the moment 67.8% test accuracy) on the novel NYT dataset for Aspect-Based Sentiment Analysis (ABSA) 
- Code (`category_classification.py`) and trained model (`category-classification-models/categ-test-acc0.677.model`) (at the moment 67.7% test accuracy) for Sentence Category Classification on the novel NYT comments dataset
- **Software that uses the trained models to infer how people feel about a specified New York Times article and supply summary statistics and plots to the user**  

#### What this project does NOT provide:
- Code for Aspect-Based Sentiment Analysis models. I trained the AEN-BERT model (and other models for experiments) using https://github.com/songyouwei/ABSA-PyTorch

### Usage
Install required packages
```
pip install -r requirements.txt
```
```
python -m spacy download en   
```
```
python -m textblob.download_corpora  
```
Help output of the script
```
python opinion_statistics.py -h
usage: opinion_statistics.py [-h] [--URL URL] [--o] [--verbose]

optional arguments:
  -h, --help  show this help message and exit
  --URL URL   New York Times Article URL
  --o         [Optional flag] Output statistics summary and graphs
              to directory
  --verbose   [Optional flag] Verbose output to the console
```
Example usage; this will create a directory with the title of the article as it appears in the URL and store outputs, summaries and graphs in the folder. It will further output the same information to stdout.

Note: The URL has to be from a recent article as [it seems](https://help.nytimes.com/hc/en-us/articles/115014792387-Comments) after 24 hours the comments are disabled.
```
python opinion_statistics.py --URL https://www.nytimes.com/2020/05/23/us/coronavirus-government-trust.html --o --verbose
```
### Datasets

The author manually annotated 2059 sentences of comments under various New York Times articles into 5 broad categories (CONTENT, MISC, PERSONAL, AUTHOR, QUALITY). The specific categories chosen are an attempt to capture most topics talked about in comment sections under news articles.

Roughly, the categories mean the following:

CONTENT - the comment is referring to the content of the article  <br />
PERSONAL - the comment is referring to a personal experience of the comment author  <br />
AUTHOR - the comment is directly referring to the author of the article  <br />
QUALITY - the comment is referring to the quality of the article  <br />
MISC - the comment is making an observation not directly related to any categories listed above

Refer to `datasets/README.md` for more information about the datasets found in this project.

### Software pipeline

After running `opinion_statistics.py` the following steps are taken by the software:

1. Retrieve comments from The New York Times using the given article's URL
2. Process them
3. Use the trained sentence category classification model to classify sentences into the 5 predefined categories
4. Use [TextBlob](https://github.com/sloria/TextBlob) (or when none found, [spaCy](https://github.com/explosion/spaCy/)) to extract noun phrases to be later used as aspect terms  
5. Use TextBlob to compute sentence-level (category-level) sentiment
6. Use the trained ABSA model and the extracted aspect terms to compute aspect-level sentiment
7. Retrieve the computed statistics parse them, make plots, output them to stdout and/or to directory   

### Training models

If you would like to train the sentence category classification model, you also need to download GloVe embeddings from https://nlp.stanford.edu/data/glove.6B.zip and extract it into the repository directory.
Refer to `category_classification.py` for more information about training, it is quite well commented.

If you would like to train the model for ABSA, refer to this repository: https://github.com/songyouwei/ABSA-PyTorch. You can also take a look at my fork, where I have added the NYT dataset https://gitlab.com/p-skaisgiris/absa-pytorch-nyt. 

### TODO:
- Add the ability to select aspect terms in the annotation script
- Conduct further experiments and/or implement different strategies from papers to increase the test accuracy of both ABSA and sentence category classification models
- Implement more computational linguistics statistics useful for users to understand how people feel and comment about the article
- Optimize software (clean redundant code, make so that the software uses less required packages (e.g. only PyTorch not keras also))
- Enrich the dataset using data augmentation
- Enrich the dataset using manual annotation
