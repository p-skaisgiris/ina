"""
    File that trains a model to classify a comment sentence to 5 broad categories
    [CONTENT, PERSONAL, AUTHOR, QUALITY, MISC] the comment is about.
    These categories are an attempt to catch all content comments take under
    news articles

    CONTENT - the comment is referring to the content of the article
    PERSONAL - the comment is referring to a personal experience of the
               comment author
    AUTHOR - the comment is directly referring to the author of the article
    QUALITY - the comment is referring to the quality of the article
    MISC - the comment is making an observation not directly related to any
           categories listed above
"""
import math
import spacy
from utils.data_util import read_nytimes_data, get_predefined_categories
import numpy as np
from sklearn.preprocessing import LabelEncoder
import joblib

from keras.models import Sequential
from keras.layers import Dense, Activation, Embedding, LSTM,\
    Conv1D, GlobalMaxPooling1D, Flatten, Lambda, Dropout, MaxPooling1D
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical

nlp = spacy.load('en')

dataset_path = "datasets/CommentsFull.txt"

print("Reading data...")
# Read data from the dataset (the data is already shuffled)
article_ids, comments, aspect_categories, sentiments = read_nytimes_data(dataset_path)

# Split data into training and testing sets (75% training, 25% testing)
training_length = math.ceil(len(article_ids) * 0.75)

# train_article_ids = article_ids[:training_length]
train_comments = comments[:training_length]
train_aspect_categories = aspect_categories[:training_length]
# train_aspect_terms = aspect_terms[:training_length]
# train_sentiments = article_ids[:training_length]

# test_article_ids = article_ids[training_length:]
test_comments = comments[training_length:]
test_aspect_categories = aspect_categories[training_length:]
# test_aspect_terms = aspect_terms[training_length:]
# test_sentiments = article_ids[training_length:]

# Fit a tokenizer on our text
tk = Tokenizer()
tk.fit_on_texts(train_comments)
vocab_length = len(tk.word_index) + 1

# Get the longest length of a sentence/comment
# SEQ_LEN = len(max(train_aspect_terms, key=len))
SEQ_LEN = len(max(train_comments, key=len))

aspect_categories_class_size = len(get_predefined_categories())

# Maximum length is the longest sentence we have in any of the sets
train_input = pad_sequences(tk.texts_to_sequences(train_comments), maxlen=SEQ_LEN)
test_input = pad_sequences(tk.texts_to_sequences(test_comments), maxlen=SEQ_LEN)

# Encode the aspect categories
label_encoder = LabelEncoder()
train_integer_category = label_encoder.fit_transform(train_aspect_categories)
test_integer_category = label_encoder.transform(test_aspect_categories)
train_labels = to_categorical(train_integer_category, num_classes=aspect_categories_class_size)
test_labels = to_categorical(test_integer_category, num_classes=aspect_categories_class_size)

# ------- Craft an embedding matrix using GloVe embeddings ---------
# This conversion is taken from https://machinelearningmastery.com/use-word-embedding-layers-deep-learning-keras/

# Dimension of the embedding (in our case 50, because we are using a 50-dimensional GloVe embedding)
EMBEDDING_DIM = 50

print("Loading GloVe vectors...")
embeddings_index = dict()
# Open a _locally_ downloaded version of GloVe model
f = open('glove.6B.50d.txt')
for line in f:
    values = line.split()
    word = values[0]
    coefs = np.asarray(values[1:], dtype='float32')
    embeddings_index[word] = coefs
f.close()
print('Loaded %s word vectors.' % len(embeddings_index))

# create a weight matrix for words in training docs
embedding_matrix = np.zeros((vocab_length, EMBEDDING_DIM))
for word, i in tk.word_index.items():
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        embedding_matrix[i] = embedding_vector

# ------------ Build the keras model --------------

# Uncomment to run multiple experiences with varying Convolution layers and varying sizes
"""
best_acc = 0
best_model = None
config = ""

layer_sizes = [32, 61, 128, 512]
cnn_layers = [1, 2, 3, 4, 5]
dense_layers = [1, 2, 3, 4, 5]
kernel_sizes = [3, 5, 7, 10]

for layer_size in layer_sizes:
    for dense_layer in dense_layers:
        for cnn_layer in cnn_layers:
            for kernel_size in kernel_sizes:
                model = Sequential()
                model.add(Embedding(vocab_length, EMBEDDING_DIM, weights=[embedding_matrix], input_length=SEQ_LEN))

                #model.add(Dropout(0.5))

                for i in range(cnn_layer):
                    model.add(Conv1D(layer_size, kernel_size, activation='relu'))
                    model.add(MaxPooling1D())

                # TODO: experiment with pool sizes

                for i in range(dense_layer):
                    model.add(Dense(layer_size, activation='relu'))

                model.add(Flatten())
                model.add(Dense(10, activation='relu'))
                #aspect_categories_model.add(Dense(512, activation='relu'))
                #model.add(LSTM(100))
                #model.add(LSTM(100))
                model.add(Dense(aspect_categories_class_size, activation='softmax'))

                model.summary()

                model.compile(loss='categorical_crossentropy',
                                                optimizer='adam',
                                                metrics=['accuracy'])

                print('Training...')
                model.fit(train_input, train_labels,
                          epochs=20,
                          validation_split=0.1,
                          verbose=False)

                print('Evaluating...')
                loss, acc = model.evaluate(test_input, test_labels)
                print('Test loss / test accuracy = {:.4f} / {:.4f}'.format(loss, acc))

                if acc > best_acc:
                    best_acc = acc
                    best_model = model
                    best_config = ""

# Save the best model
best_model.save('category-classification-models/categ-test-acc-{:.4f}.model'.format(best_acc))
#joblib.dump(label_encoder, 'models/acsa.enc')
#joblib.dump(tk, 'models/acsa.token')
"""

model = Sequential()
model.add(Embedding(vocab_length, EMBEDDING_DIM, weights=[embedding_matrix], input_length=SEQ_LEN))
model.add(Conv1D(512, 5, activation='relu'))
model.add(MaxPooling1D())
model.add(Conv1D(512, 5, activation='relu'))
model.add(MaxPooling1D())
model.add(Dense(512, activation='relu'))
model.add(Dense(512, activation='relu'))
model.add(Flatten())
model.add(Dense(10, activation='relu'))
model.add(Dense(aspect_categories_class_size, activation='softmax'))
model.compile(optimizer='adam',
              loss='binary_crossentropy',
              metrics=['accuracy'])
model.summary()

model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

print('Training...')
model.fit(train_input, train_labels,
          epochs=20,
          validation_split=0.1)

print('Evaluating...')
loss, acc = model.evaluate(test_input, test_labels)
print('Test loss / test accuracy = {:.4f} / {:.4f}'.format(loss, acc))

model.save('category-classification-models/categ-test-acc-{:.4f}.model'.format(acc))
joblib.dump(label_encoder, 'category-classification-models/acsa.enc')
joblib.dump(tk, 'category-classification-models/acsa.token')


# Define new comments to test the model on
new_comments = ["Guns should be banned they are really bad",
                "I really enjoyed this article!",
                "Why does the media not highlight more than her lack of experience and huge (some might say BIGLY) "
                "political contributions to Republican candidates and Party?",
                "This author must be out of his mind",
                "I remember this fair when I was a child"]

# Preprocess the new data the same way it was preprocessed for the model, predict the outcome and transform back
# to human-redable form
for comm in new_comments:
    print()
    print(comm)
    new_review_aspect_tokenized = pad_sequences(tk.texts_to_sequences([comm]), maxlen=SEQ_LEN)

    new_review_category = label_encoder.inverse_transform(model.predict_classes(new_review_aspect_tokenized))
    print(new_review_category)
