# Suppress warnings useless for users
import warnings
warnings.filterwarnings('ignore', category=DeprecationWarning)
warnings.filterwarnings('ignore', category=FutureWarning)
warnings.filterwarnings('ignore', category=UserWarning)
import os
# Suppress TensorFlow output
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import argparse
from time import time
from pathlib import Path
from utils.data_util import get_comments_nytimes,\
    retrieve_predicted
import joblib
from keras.models import load_model
import re
from collections import defaultdict
import matplotlib.pyplot as plt
plt.style.use('seaborn')


def compute_aspect_terms_stats(aspect_terms, aspect_sentiments, verbose, output, dir_path, total_comments):
    aspect_stats = defaultdict(lambda: defaultdict(lambda: 0.0))
    aspect_stats_file = None
    if output:
        aspect_stats_file = open("{}aspect_terms_summary.txt".format(dir_path), 'w')
        aspect_stats_file.write("Total sentences analyzed: {}\n".format(total_comments))

    for i in range(len(aspect_terms)):
        aspect_term = aspect_terms[i]
        aspect_sentiment = aspect_sentiments[i]

        aspect_stats[aspect_term]['freq'] += 1
        aspect_stats[aspect_term]['sent'] += aspect_sentiment
        aspect_stats[aspect_term]['rel_sent'] = aspect_stats[aspect_term]['sent'] / aspect_stats[aspect_term]['freq']

    most_frequent_aspects = sorted(aspect_stats.items(), key=lambda k_v: k_v[1]['freq'], reverse=True)

    top_10_frequent = "Top 10 most frequent aspect terms used by commenters for this article:\n"
    if verbose:
        print(top_10_frequent)
    if output:
        aspect_stats_file.write(top_10_frequent)

    count = 0
    for key, value in most_frequent_aspects:
        out1 = "Aspect: {}\n".format(key)
        out2 = "Frequency: {}\n".format(value['freq'])
        out3 = "Absolute sentiment score: {}\n".format(value['sent'][0])
        out4 = "Relative sentiment score: {:.4f}\n".format(value['rel_sent'][0])

        if verbose:
            print(out1)
            print(out2)
            print(out3)
            print(out4)
        if output:
            aspect_stats_file.write(out1)
            aspect_stats_file.write(out2)
            aspect_stats_file.write(out3)
            aspect_stats_file.write(out4)

        count += 1
        if count == 10:
            break

    top_10_frequent_pos = "Top 10 most frequent positive aspect terms used by commenters for this article:\n"
    if verbose:
        print()
        print(top_10_frequent_pos)
    if output:
        aspect_stats_file.write("\n")
        aspect_stats_file.write(top_10_frequent_pos)

    count = 0
    for key, value in most_frequent_aspects:
        if value['sent'] > 0:
            out1 = "Aspect: {}\n".format(key)
            out2 = "Frequency: {}\n".format(value['freq'])
            out3 = "Absolute sentiment score: {}\n".format(value['sent'][0])
            out4 = "Relative sentiment score: {:.4f}\n".format(value['rel_sent'][0])

            if verbose:
                print(out1)
                print(out2)
                print(out3)
                print(out4)
            if output:
                aspect_stats_file.write(out1)
                aspect_stats_file.write(out2)
                aspect_stats_file.write(out3)
                aspect_stats_file.write(out4)
            count += 1
        if count == 10:
            break

    top_10_frequent_neg = "Top 10 most frequent negative aspect terms used by commenters for this article:\n"
    if verbose:
        print()
        print(top_10_frequent_neg)
    if output:
        aspect_stats_file.write("\n")
        aspect_stats_file.write(top_10_frequent_neg)

    count = 0
    for key, value in most_frequent_aspects:
        if value['sent'] < 0:
            out1 = "Aspect: {}\n".format(key)
            out2 = "Frequency: {}\n".format(value['freq'])
            out3 = "Absolute sentiment score: {}\n".format(value['sent'][0])
            out4 = "Relative sentiment score: {:.4f}\n".format(value['rel_sent'][0])

            if verbose:
                print(out1)
                print(out2)
                print(out3)
                print(out4)
            if output:
                aspect_stats_file.write(out1)
                aspect_stats_file.write(out2)
                aspect_stats_file.write(out3)
                aspect_stats_file.write(out4)

            count += 1
        if count == 10:
            break


def compute_categories_stats(category_stats, verbose, output, dir_path, total_comments):
    category_info = defaultdict(lambda: [])
    category_stats_file = None
    if output:
        category_stats_file = open("{}category_stats_summary.txt".format(dir_path), 'w')
        category_stats_file.write("Total sentences analyzed: {}\n".format(total_comments))

    for _, category, category_sentiment in category_stats:
        category_info[category[0  ]].append(category_sentiment)

    cat_info = "Information about categories:\n"
    if verbose:
        print()
        print(cat_info)
    if output:
        category_stats_file.write(cat_info)

    shares = []
    for key in category_info:
        length = len(category_info[key])
        shares.append(length / len(category_stats))
        cat_sum = sum(category_info[key])
        out1 = "Category: {}\n".format(key)
        out2 = "Frequency: {}\n".format(length)
        out3 = "Absolute sentiment score: {}\n".format(cat_sum)
        out4 = "Relative sentiment score: {:.4f}\n".format(cat_sum / length)
        if verbose:
            print(out1)
            print(out2)
            print(out3)
            print(out4)
        if output:
            category_stats_file.write(out1)
            category_stats_file.write(out2)
            category_stats_file.write(out3)
            category_stats_file.write(out4)

    labels = list(category_info.keys())
    fig, ax = plt.subplots()
    ax.pie(shares, labels=labels, autopct='%1.2f%%')
    ax.set_title("Percentage of categories talked about in comments")
    if output:
        plt.savefig("{}percentage-categories.png".format(dir_path))
    if verbose:
        plt.show()

    positive_counts = [category_info[key].count(1) / len(category_info[key]) for key in category_info]
    negative_counts = [category_info[key].count(-1) / len(category_info[key]) for key in category_info]
    neutral_counts = [category_info[key].count(0) / len(category_info[key]) for key in category_info]

    width = 0.35
    fig2, ax2 = plt.subplots()
    ax2.bar(labels, negative_counts, width, label='% Negative')
    ax2.bar(labels, neutral_counts, width, bottom=negative_counts,
            label='% Neutral')
    ax2.bar(labels, positive_counts, width, bottom=[a + b for a, b in zip(negative_counts, neutral_counts)],
            label='% Positive')
    ax2.set_ylabel('%')
    ax2.set_title('Percentage of sentiment for each category')
    ax2.legend()

    if output:
        plt.savefig("{}percentage-sentiment-categories.png".format(dir_path))
    if verbose:
        plt.show()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--URL', type=str, help="New York Times Article URL", default="https://www.nytimes.com/2020/05/22/nyregion/beaches-ny-coronavirus.html")
    parser.add_argument('--o', action='store_true', help="[Optional flag] Output statistics summary and graphs to directory")
    parser.add_argument('--verbose', action='store_true', help="[Optional flag] Verbose output to the console")
    args = parser.parse_args()

    if not args.o and not args.verbose:
        print("Please either select the --verbose option or --o for output option, otherwise there will be no output")
        return

    # Retrieve all comments from the given URL and segment them into sentences
    comments = get_comments_nytimes(args.URL)

    if len(comments) == 0:
        print("There were no comments found. It might be because the article you are interested in did not have comments enabled. Please give another URL.")
        return

    # Load pre-trained sentence classification model as well as
    # label encoder and tokenizer used to train the model
    model = load_model("category-classification-models/categ-test-acc-0.677.model")
    label_encoder = joblib.load("category-classification-models/categ.enc")
    tokenizer = joblib.load("category-classification-models/categ.token")

    # Use the pretrained models to predict categories, aspect term sentiment for each sentence
    # Also use TextBlob to predict sentence-level sentiment
    category_stats, aspect_terms,\
    aspect_term_sentiments, bert_prepared_sentences = retrieve_predicted(model, label_encoder, tokenizer, comments)

    category_stats_file = None
    aspect_stats_file = None
    directory_path = None

    if args.o:
        title = re.sub(r"\.html.*", "", args.URL)
        title = title.split('/')[-1]
        directory_path = "{}-{}/".format(title, int(time()))
        Path(directory_path).mkdir()
        category_stats_file = open("{}category_output.txt".format(directory_path), 'w')
        aspect_stats_file = open("{}aspect_output.txt".format(directory_path), 'w')

    for comment, category, category_sentiment in category_stats:
        comment_formatted = "Comment: {}".format(comment)
        category_formatted = "Category: {}, its sentiment {}".format(category, category_sentiment)
        if args.verbose:
            print()
            print(comment_formatted)
            print(category_formatted)
        if args.o:
            category_stats_file.write("{}\n".format(comment.strip()))
            category_stats_file.write("{}\n".format(category[0]))
            category_stats_file.write("{}\n".format(category_sentiment))

    for i in range(len(aspect_terms)):
        # Check if all of the parameters are in the way we expect them to be
        if len(bert_prepared_sentences[i]) != 3 or aspect_terms[i] == '' or len(aspect_term_sentiments[i]) != 1:
            continue

        comment_formatted = "Sentence: {} {} {}".format(bert_prepared_sentences[i][0],
                                                        bert_prepared_sentences[i][1],
                                                        bert_prepared_sentences[i][2])
        aspect_formatted = "Aspect term: {}, its sentiment {}".format(aspect_terms[i], aspect_term_sentiments[i][0])

        if args.verbose:
            print()
            print(comment_formatted)
            print(aspect_formatted)
        if args.o:
            aspect_stats_file.write("{} {} {}\n".format(bert_prepared_sentences[i][0],
                                                        bert_prepared_sentences[i][1],
                                                        bert_prepared_sentences[i][2].strip()))
            aspect_stats_file.write("{}\n".format(aspect_terms[i]))
            aspect_stats_file.write("{}\n".format(aspect_term_sentiments[i][0]))

    compute_aspect_terms_stats(aspect_terms, aspect_term_sentiments, args.verbose, args.o, directory_path, len(comments))
    compute_categories_stats(category_stats, args.verbose, args.o, directory_path, len(comments))

if __name__ == '__main__':
    main()
