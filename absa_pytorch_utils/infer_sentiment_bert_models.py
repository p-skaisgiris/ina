# -*- coding: utf-8 -*-
# file: infer_example_bert_models.py
# author: songyouwei <youwei0314@gmail.com>
# fixed: yangheng <yangheng@m.scnu.edu.cn>
# Copyright (C) 2018. All Rights Reserved.

"""
    File that predicts the sentiment of a given aspect in a sentence.

    This file is a modified version of infer_example_bert_models.py
    and data_utils.py from https://github.com/songyouwei/ABSA-PyTorch
"""

import numpy as np
import torch
import torch.nn.functional as F
#from models.lcf_bert import LCF_BERT
from absa_pytorch_utils.bert_models.aen import AEN_BERT
#from models.bert_spc import BERT_SPC
from pytorch_transformers import BertModel
#from data_utils import Tokenizer4Bert
import argparse
from pytorch_transformers import BertTokenizer


class Tokenizer4Bert:
    def __init__(self, max_seq_len, pretrained_bert_name):
        self.tokenizer = BertTokenizer.from_pretrained(pretrained_bert_name)
        self.max_seq_len = max_seq_len

    def text_to_sequence(self, text, reverse=False, padding='post', truncating='post'):
        sequence = self.tokenizer.convert_tokens_to_ids(self.tokenizer.tokenize(text))
        if len(sequence) == 0:
            sequence = [0]
        if reverse:
            sequence = sequence[::-1]
        return pad_and_truncate(sequence, self.max_seq_len, padding=padding, truncating=truncating)


def pad_and_truncate(sequence, maxlen, dtype='int64', padding='post', truncating='post', value=0):
    x = (np.ones(maxlen) * value).astype(dtype)
    if truncating == 'pre':
        trunc = sequence[-maxlen:]
    else:
        trunc = sequence[:maxlen]
    trunc = np.asarray(trunc, dtype=dtype)
    if padding == 'post':
        x[:len(trunc)] = trunc
    else:
        x[-len(trunc):] = trunc
    return x


def prepare_data(text_left, aspect, text_right, tokenizer):
    text_left = text_left.lower().strip()
    text_right = text_right.lower().strip()
    aspect = aspect.lower().strip()
    
    text_raw_indices = tokenizer.text_to_sequence(text_left + " " + aspect + " " + text_right)            
    aspect_indices = tokenizer.text_to_sequence(aspect)
    aspect_len = np.sum(aspect_indices != 0)
    text_bert_indices = tokenizer.text_to_sequence('[CLS] ' + text_left + " " + aspect + " " + text_right + ' [SEP] ' + aspect + " [SEP]")
    text_raw_bert_indices = tokenizer.text_to_sequence(
        "[CLS] " + text_left + " " + aspect + " " + text_right + " [SEP]")
    bert_segments_ids = np.asarray([0] * (np.sum(text_raw_indices != 0) + 2) + [1] * (aspect_len + 1))
    bert_segments_ids = pad_and_truncate(bert_segments_ids, tokenizer.max_seq_len)
    aspect_bert_indices = tokenizer.text_to_sequence("[CLS] " + aspect + " [SEP]")

    return text_bert_indices, bert_segments_ids, text_raw_bert_indices, aspect_bert_indices


def infer_aspect_sentiments(sentences):
    """
        Input: list of sentences that are split as follows:
        <Left side of sentence>, <Aspect term>, <Right side of sentence>.
        So, the list looks this: [[<Left side of sentence>, <Aspect term>, <Right side of sentence>], ...]

        Returns: sentiment polarities towards the given aspect terms in the given sentence
    """

    model_classes = {
        # 'bert_spc': BERT_SPC,
        'aen_bert': AEN_BERT,
        # 'lcf_bert': LCF_BERT
    }
    # set your trained models here
    state_dict_paths = {
        # 'lcf_bert': 'state_dict/lcf_bert_laptop_val_acc0.2492',
        # 'bert_spc': 'state_dict/bert_spc_laptop_val_acc0.268',
        'aen_bert': 'absa_pytorch_utils/pretrained_bert_model/aen_bert_nyt_test_acc0.678'
    }

    # opt = get_parameters()

    # Parameters that our model was created with
    opt = argparse.ArgumentParser()
    opt.model_name = 'aen_bert'
    opt.max_seq_len = 80
    opt.dropout = 0.1
    opt.hidden_dim = 300
    opt.bert_dim = 768
    opt.polarities_dim = 3
    opt.pretrained_bert_name = 'bert-base-uncased'
    opt.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    tokenizer = Tokenizer4Bert(opt.max_seq_len, opt.pretrained_bert_name)
    bert = BertModel.from_pretrained(opt.pretrained_bert_name)
    model = model_classes[opt.model_name](bert, opt).to(opt.device)
    
    # print('loading model {0} ...'.format(opt.model_name))
    model.load_state_dict(torch.load(state_dict_paths[opt.model_name]))
    model.eval()
    torch.autograd.set_grad_enabled(False)

    # input: This little place has a cute interior decor and affordable city prices.
    # text_left = This little place has a cute 
    # aspect = interior decor
    # text_right = and affordable city prices.

    aspect_sentiments = []

    for left_side, aspect, right_side in sentences:
        text_bert_indices, bert_segments_ids, text_raw_bert_indices, aspect_bert_indices = \
            prepare_data(left_side, aspect, right_side, tokenizer)

        # text_bert_indices = torch.tensor([text_bert_indices], dtype=torch.int64).to(opt.device)
        # bert_segments_ids = torch.tensor([bert_segments_ids], dtype=torch.int64).to(opt.device)
        text_raw_bert_indices = torch.tensor([text_raw_bert_indices], dtype=torch.int64).to(opt.device)
        aspect_bert_indices = torch.tensor([aspect_bert_indices], dtype=torch.int64).to(opt.device)
        inputs = [text_raw_bert_indices, aspect_bert_indices]
        # if 'lcf' in opt.model_name:
        #    inputs = [text_bert_indices, bert_segments_ids, text_raw_bert_indices, aspect_bert_indices]
        # elif 'aen' in opt.model_name:
        #    inputs = [text_raw_bert_indices, aspect_bert_indices]
        # elif 'spc' in opt.model_name:
        #    inputs = [text_bert_indices, bert_segments_ids]
        outputs = model(inputs)

        t_probs = F.softmax(outputs, dim=-1).cpu().numpy()
        aspect_sentiment = t_probs.argmax(axis=-1) - 1
        # print('t_probs = ', t_probs)
        # print('aspect sentiment = ', t_probs.argmax(axis=-1) - 1)
        aspect_sentiments.append(aspect_sentiment)

    return aspect_sentiments

