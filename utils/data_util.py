"""
    File that holds various utility functions. Mainly retrieving/computing data.
"""
import spacy
from textblob import TextBlob
import requests
from urllib.parse import quote
import time
import re
from sklearn.utils import shuffle
from keras.preprocessing.sequence import pad_sequences
from absa_pytorch_utils.infer_sentiment_bert_models import infer_aspect_sentiments


def get_predefined_categories():
    return ["CONTENT", "AUTHOR", "QUALITY", "PERSONAL", "MISC"]


def get_predefined_sentiments():
    return [-1, 0, 1]


def read_nytimes_data(path):
    """
        Read the New York Times dataset from the provided path and parse data into lists.

        Input: path to dataset
        Returns: article IDs (could be used to retrieve additional data about the article associated with the comment),
                 comment sentences,
                 categories the comments were annotated as,
                 corresponding sentiments towards each comment's topic
    """
    # Load spaCy
    nlp = spacy.load('en', tagger=False, ner=False, textcat=False)

    article_ids = []
    comments = []
    aspect_categories = []
    aspect_terms = []
    sentiments = []

    # The format of the NYT dataset is as follows:
    # ID: <Article ID>
    # <Comment>
    # <Aspect category>
    # <Sentiment polarity>
    # ID: <Article ID>
    # ...
    with open(path, 'r') as f:
        while True:
            id = f.readline().strip()

            # FIXME: not very robust?
            # End of file
            if id == '':
                break

            # Remove "ID: "
            id = id[4:]
            article_ids.append(id)

            sentence = f.readline().strip()
            sentence = sentence.lower()
            comments.append(sentence)

            aspect_category = f.readline().strip()
            aspect_categories.append(aspect_category)

            # Take noun chunks from the sentence to be used as aspect terms
            noun_phrases = compute_noun_phrases(sentence, nlp)
            for noun_phrase in noun_phrases:
                aspect_terms.append(noun_phrase)

            sentiment = int(f.readline())
            sentiments.append(sentiment)

    # TODO: decide if aspect terms and article ids need to stay, at the moment they are not used

    # Shuffle the data so all patterns are lost
    article_ids, comments, aspect_categories, sentiments = shuffle(article_ids, comments,
                                                                   aspect_categories,
                                                                   sentiments)
    # return article_ids, comments, aspect_categories, aspect_terms, sentiments
    return article_ids, comments, aspect_categories, sentiments


def compute_noun_phrases(sentence, nlp):
    def parse_noun_phrases(aspect_terms, noun_phrases):
        for noun_phrase in noun_phrases:
            left_right_sides = sentence.lower().split(noun_phrase, 1)

            # Skip if not split correctly
            if len(left_right_sides) != 2:
                continue

            # Retrieve the original non-lowercased text
            left_right_sides = [sentence[:len(left_right_sides[0])],
                                sentence[-len(left_right_sides[1]):]]
            non_lower_case_noun_phrase = sentence[len(left_right_sides[0]):
                                                  len(left_right_sides[0]) +
                                                  len(noun_phrase)]
            aspect_terms.append(non_lower_case_noun_phrase)

    aspect_terms = []
    blob_sentence = TextBlob(sentence)
    noun_phrases = blob_sentence.noun_phrases
    if len(noun_phrases) == 0:
        # TODO: investigate if noun chunks are lowercase for spacy
        # Fallback procedure in case there are no noun phrases found by TextBlob
        noun_phrases = [(chunk.root.text) for chunk in nlp(sentence).noun_chunks if chunk.root.pos_ == 'NOUN']
        parse_noun_phrases(aspect_terms, noun_phrases)
    else:
        parse_noun_phrases(aspect_terms, noun_phrases)

    return noun_phrases


def get_comments_nytimes(url_given):
    # Load spaCy
    nlp = spacy.load('en', tagger=False, ner=False, textcat=False)

    def parse_and_clean_comments(comments, all_comments):
        for c in comments:
            # TODO: investigate if TextBlob is faster and/or better for sentence segmentation
            tokens = nlp(c['commentBody'])
            # Split whole comment into sentences
            sentences = [sent.text for sent in tokens.sents]
            for sentence in sentences:
                # Filter out sentences that are less than 3 tokens or more than 40 tokens
                if 3 <= len(sentence.split()) < 40:
                    # Remove links
                    sentence = re.sub(r'\<a href="https.*>', '', sentence)
                    # Remove newlines
                    sentence = sentence.replace('\n', '')
                    all_comments.append(sentence.strip())

    # Remove unnecessary things after .html
    cleaned = re.sub(r"\.html.*", ".html", url_given)

    if "www.nytimes.com" not in cleaned:
        raise ValueError("This URL is invalid. Please send a valid New York Times URL.")

    url = quote(cleaned)
    offset = 0
    limit = 1000
    all_comments = []
    r = requests.get(
        f"https://www.nytimes.com/svc/community/V3/requestHandler?url={url}&method=get&commentSequence=0&offset={offset}&includeReplies=true&sort=newest&cmd=GetCommentsAll&limit={limit}")

    data = r.json()
    results = data['results']
    total_found = results['totalCommentsFound']
    comments = results['comments']
    parse_and_clean_comments(comments, all_comments)

    offset += limit

    while offset < total_found:
        # A safety net so we wouldn't get flagged as a spammer
        time.sleep(1)
        r = requests.get(
            f"https://www.nytimes.com/svc/community/V3/requestHandler?url={url}&method=get&commentSequence=0&offset={offset}&includeReplies=true&sort=newest&cmd=GetCommentsAll&limit={limit}")
        data = r.json()
        comments = data['results']['comments']
        parse_and_clean_comments(comments, all_comments)
        offset += limit

    return all_comments


def retrieve_predicted(model, label_encoder, tokenizer, comments):
    # Load spaCy
    nlp = spacy.load('en', tagger=False, ner=False, textcat=False)

    def parse_noun_phrases(noun_phrases, aspect_term_list, bert_prepared_sentences):
        for noun_phrase in noun_phrases:
            # Prepare the sentences for BERT ABSA
            # Format: <Left side of sentence>, <Aspect term>, <Right side of sentence>.
            left_right_sides = comm.lower().split(noun_phrase, 1)

            # Skip if not split correctly
            if len(left_right_sides) != 2:
                continue

            # Retrieve the original non-lowercased text
            left_right_sides = [comm[:len(left_right_sides[0])],
                                comm[-len(left_right_sides[1]):]]
            non_lower_case_noun_phrase = comm[len(left_right_sides[0]):len(left_right_sides[0]) + len(noun_phrase)]

            aspect_term_list.append(non_lower_case_noun_phrase)
            bert_prepared_sentence = [left_right_sides[0][:-1], non_lower_case_noun_phrase, left_right_sides[1][1:]]
            bert_prepared_sentences.append(bert_prepared_sentence)

    category_stats = []
    aspect_term_list = []
    bert_prepared_sentences = []
    # Threshold that is used for sentiment analysis
    # The higher the threshold, more certain the model has to be about the sentence's
    # sentiment score. Also means that more sentences are classified as neutral
    sentiment_threshold = 0.3

    for comm in comments:
        # Collect aspect terms for Aspect-Based Sentiment Analysis
        blob_sentence = TextBlob(comm)
        noun_phrases = blob_sentence.noun_phrases
        if len(noun_phrases) == 0:
            # Fallback procedure in case there are no noun phrases found by TextBlob
            noun_phrases = [(chunk.root.text) for chunk in nlp(comm).noun_chunks if chunk.root.pos_ == 'NOUN']
            parse_noun_phrases(noun_phrases, aspect_term_list, bert_prepared_sentences)
        else:
            parse_noun_phrases(noun_phrases, aspect_term_list, bert_prepared_sentences)

        # Prepare the sentence in the same way that the model was trained
        # 486 is the longest length of a sentence appearing in NYT training data
        # (this length was used in training as well and has to be used here to do
        # inverse transform correctly)
        # TODO: cache this length somehow
        try:
            comm_tokenized = pad_sequences(tokenizer.texts_to_sequences([comm]), maxlen=486)
            # Predict the category of the sentence
            category = label_encoder.inverse_transform(model.predict_classes(comm_tokenized))
        except Exception:
            # print("There has been an exception while classifying the category.\nSentence: {}".format(comm))
            continue

        # Infer the sentence-level sentiment (category-level sentiment)
        blob_sent = 0
        if blob_sentence.sentiment.polarity >= sentiment_threshold:
            blob_sent = 1
        elif sentiment_threshold > blob_sentence.sentiment.polarity >= -sentiment_threshold:
            blob_sent = 0
        else:
            blob_sent = -1

        category_stats.append([comm, category, blob_sent])

    aspect_term_sentiments = infer_aspect_sentiments(bert_prepared_sentences)

    return category_stats, aspect_term_list, aspect_term_sentiments, bert_prepared_sentences
