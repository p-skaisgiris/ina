import pandas as pd
import spacy
from time import time
from utils.data_util import get_predefined_categories


def update_skip_row_file(skip_rows_path, skip_rows):
    # Overwrite the file
    with open(skip_rows_path, "w") as skip_file:
        skip_file.write(str(skip_rows))


nlp = spacy.load('en', parser=False, entity=False)

# Amount of dataset rows to load
NROWS = 200

# Path to the dataset to annotate
comments_path = "../data/CommentsApril2018.csv"
articles_path = "../data/ArticlesApril2018.csv"

# TODO: create a better pipeline for this, multiple locations should be checked
# Try opening the file that contains how many rows to skip (already annotated data)
# if not found, no data is skipped
SKIP_ROWS_PATH = "{}_skip_rows".format(comments_path)
try:
    skip = open(SKIP_ROWS_PATH, 'r')
    SKIP_ROWS = int(skip.readline())
except FileNotFoundError:
    SKIP_ROWS = 0

# Read NROWS lines of the data set at a time
comments_df = pd.read_csv(comments_path, skiprows=range(1, SKIP_ROWS+1), nrows=NROWS)
# Read all of the articles data set as it's much smaller
articles_df = pd.read_csv(articles_path)
# Filter by the relevant columns
comments_df = comments_df.loc[:, ["articleID", "commentBody"]]
articles_df = articles_df.loc[:, ["articleID", "byline", "headline", "keywords"]]

# Define topics to filter out (if you think you have annotated too much of some topic
# already and would like to balance the dataset)
unwanted_topics = ['United States Politics and Government']

custom_df = comments_df
# Filter out specific topics
for index, row in comments_df.iterrows():
    # Get article this comment is associated with
    article = articles_df[articles_df.articleID == row.articleID]
    hit = False
    keywords = ' '.join(article.keywords)
    # Look if this article's keywords contains the topics you do not want
    for topic in unwanted_topics:
        if topic in keywords:
            hit = True
            break
    # If the unwanted topic exists, filter out these comments
    if hit:
        id = article.articleID.values[0]
        custom_df = custom_df[custom_df.articleID != id]

comments_df = custom_df

# Define these categories in utils/data_util
predefined_categories = get_predefined_categories()
# Predefined sentiment values
predefined_sentiments = ['-1', '0', '1']

if len(comments_df) == 0:
    print("There is no data to annotate, please inspect the data and/or dataframe")
    update_skip_row_file(SKIP_ROWS_PATH, SKIP_ROWS+NROWS)
else:
    sentence_count = 0
    i = 0

    with open("{}-{}".format(comments_path, int(time())), "w") as f:
        while True:
            # Annotation session stopping flag
            exit_annot = False

            try:
                comment = comments_df.iloc[i]
            except IndexError:
                print("You have reached the end of the current annotation session")
                print("You have annotated {} entries and {} sentences".format(i, sentence_count))
                break

            print()
            print("Overall progress: [{}/{}]".format(i, len(comments_df)))

            # Get the article this comment is associated with
            article = articles_df[articles_df.articleID == comment.articleID]
            print()
            print("{}".format(article.byline.values[0]))
            print("Title:")
            print("{}".format(article.headline.values[0]))
            print("Keywords:")
            print(' '.join(article.keywords))

            # Tokenize comment
            tokens = nlp(comment.commentBody)
            # Split whole comment into sentences
            sentences = [sent.text for sent in tokens.sents]
            # Filter out sentences that are less than 3 tokens or more than 50 tokens
            # FIXME: send the sentence to be split into sentences again as sometimes it doesn't do it well enough
            # (sometimes spacy segments sentences wrongly)
            sentences = [sentence for sentence in sentences if len(sentence.split()) >= 3 and len(sentence.split()) < 40]

            # Annotate each sentence in a comment
            for sen_index in range(len(sentences)):
                sentence = sentences[sen_index]
                sentence = sentence.replace("<br/>", "")
                print()
                print("Sub-progress: [{}/{}]".format(sen_index, len(sentences)))
                print()
                print(sentence)

                while True:
                    annotation = input()

                    # End of annotation session
                    if annotation.lower() == 'exit':
                        exit_annot = True
                        break

                    annotation = annotation.split()

                    if len(annotation) != 2:
                        print("Please annotate in the following format: <CATEGORY> <SENTIMENT POLARITY>")
                        print("Where <CATEGORY> is one of [{}]".format(' '.join(predefined_categories)))
                        print("and <SENTIMENT POLARITY> is one of [{}]".format(' '.join(predefined_sentiments)))
                        print()
                        continue

                    annotation[0] = annotation[0].upper()

                    if annotation[0] not in predefined_categories:
                        print("The <CATEGORY> must be in predefined_categories: [{}]".format(' '.join(predefined_categories)))
                        print()
                        continue

                    try:
                        if annotation[1] not in predefined_sentiments:
                            print("The <SENTIMENT POLARITY> must be in predefined_sentiments: [{}]".format(' '.join(predefined_sentiments)))
                            print()
                            continue
                    except ValueError:
                        print("The <SENTIMENT POLARITY> must be in predefined_sentiments: [{}]".format(' '.join(predefined_sentiments)))
                        print()
                        continue

                    break

                if exit_annot:
                    break

                sentence_count += 1
                f.write("ID: {}\n".format(comment.articleID))
                # Tokenize the sentence because the format of other files have this
                sent_tokens = nlp.tokenizer(sentence)
                sent_token_list = [word.text for word in sent_tokens]
                f.write("{}\n".format(' '.join(sent_token_list)))
                f.write("{}\n".format(annotation[0]))
                f.write("{}\n".format(annotation[1]))

            if exit_annot:
                print("You have reached the end of the current annotation session")
                print("You have annotated {} entries and {} sentences".format(i, sentence_count))
                update_skip_row_file(SKIP_ROWS_PATH, SKIP_ROWS+i)
                break

            i += 1

# Annotation session finished successfully
update_skip_row_file(SKIP_ROWS_PATH, SKIP_ROWS+NROWS)