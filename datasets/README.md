## Datasets

This folder holds the datasets used for this project's various experiments training the Aspect-Based Sentiment Analysis models.

We also propose a novel dataset (`nyt-train.txt` and `nyt-test.txt`). This dataset originally came from [Kaggle](https://www.kaggle.com/aashita/nyt-comments).
The author manually annotated 2059 sentences of comments under various New York Times articles into 5 broad categories (CONTENT, MISC, PERSONAL, AUTHOR, QUALITY). The specific categories chosen are an attempt to capture most topics talked about in comment sections under news articles.
Furthermore, each sentence was annotated with a sentiment score (one of -1, 0, 1 for negative, neutral, positive respectively) to reflect the sentiment the comment expressed towards the topic of the sentence.

The dataset was annotated using `annotating_nyt_dataset.py` found in this repository.

The statistics of this dataset are as follows:

|Category|Negative|Neutral|Positive|Total|
|---|---|---|---|---|
|Content|332|539|84|955|
|Misc|151|481|42|674|
|Quality|7|2|20|29|
|Personal|80|222|81|383|
|Author|1|3|14|18|
| |571|1247|241|2059|

The statistics bring up a clear point about the quality of the dataset: the dataset is unbalanced. There are many more neutral samples than positive or negative and there are very few sentences talking about the quality of the article or article's author. This issue can be tackled in two ways:
- Adjust the categories. Perhaps merge the category about the quality of the article and the author as the sentences must be rather similar anyway.
- Get more samples. For one, the dataset is not very large anyway. A better dataset can be acquired by collecting more entries. If the dataset is large enough, it can be balanced by cutting out samples that there are an excess of.
- Only recently I noticed that [NYT manually approves every comment](https://help.nytimes.com/hc/en-us/articles/115014792387-Comments), thus this dataset might not be a valid representation of the public opinion as it is possibly heavily filtered.

Some other best practices for improving the dataset:
- Assign at least 3 people for annotation. The annotated entry should only occur in the final dataset if the majority of annotators agree on the annotation.
- Clearly lay out the categories and what is wanted to express by annotating the sentence in this way. Give examples. Perhaps the annotators should have constant access to a "cheat sheet" that would help them make the annotations more reliably and faster.

`existing-train.txt` is a merged dataset of SemEval's classic Laptop, Restaurant datasets (read about them [here](http://alt.qcri.org/semeval2016/task5/)) as well as Twitter dataset. `hybrid-train.txt` is a dataset that merges Laptop, Restaurant, Twitter and our novel New York Times dataset.

